const jwt = require("jsonwebtoken");
function verifyToken(req, res, next) {
  const token = req.header("Authorization");

  if (!token) {
    return res.status(401).json({ error: "Unauthorized123" });
  }
  jwt.verify(token, process.env.JWT_SECRET_KEY, (err, id) => {
    if (err) {
      return res.status(403).json({ error: "Forbidden" });
    }

    req.id = id;

    next();
  });
}

module.exports = verifyToken;
