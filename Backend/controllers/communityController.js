const pool = require("../config/db");

const sendRequest = async (req, res) => {
  const { id } = req.id;

  try {
    const { receiverId, createdAt } = req.body;

    const friend = false;
    await pool.query(
      `INSERT INTO public."Friend" (senderid, receiverid, friend, createdat) VALUES ($1, $2, $3, $4) RETURNING *`,
      [id, receiverId, friend, createdAt]
    );

    res.json({
      success: true,
      message: "Request Sent successfully",
    });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};

const removeRequest = async (req, res) => {
  try {
    const { senderId, receiverId } = req.body;

    await pool.query(
      `DELETE FROM public."Friend" WHERE senderid=$1 AND receiverid=$2`,
      [senderId, receiverId]
    );

    res.json({
      success: true,
      message: "Request Removed successfully",
    });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ success: false, message: error.message });
  }
};

const acceptRequest = async (req, res) => {
  const { id } = req.id;

  try {
    const { senderid } = req.body;
    await pool.query(
      `UPDATE public."Friend" SET friend = true WHERE senderid=$1 AND receiverid=$2`,
      [senderid, id]
    );

    res.json({
      success: true,
      message: "Request Accepted successfully",
    });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ success: false, message: error.message });
  }
};

const removeConnection = (req, res) => {
  const { id } = req.id;
  try {
    const { id2 } = req.body;

    pool.query(
      `DELETE FROM "Friend"
      WHERE senderid IN ($1, $2) AND receiverid IN ($1, $2)`,
      [id, id2]
    );

    res.json({
      success: true,
      message: "Request Removed successfully",
    });
  } catch (error) {
    console.error(error.message);
    res.status(500).json({ success: false, message: error.message });
  }
};

const visitUser = async (req, res) => {
  const { userid } = req.params;
  const { id } = req.id;
  try {
    const activities = await pool.query(
      `SELECT a.*, u.firstname, u.lastname, f.friend FROM public."Activities" as a 
      JOIN public."User" AS u on u.id = a.createdby
      LEFT JOIN public."Friend" as f on (senderid in ($1,$2) AND receiverid in ($1,$2))
      WHERE createdby = $1 and f.friend = true and timeline = true
      ORDER BY createdat DESC`,
      [userid, id]
    );

    res.json({ success: true, activity: activities.rows });
  } catch (error) {
    res.status(404).json({ success: false, message: "User not Found!" });
  }
};

module.exports = {
  sendRequest,
  removeRequest,
  acceptRequest,
  removeConnection,
  visitUser,
};
