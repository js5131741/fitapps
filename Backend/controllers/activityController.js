const pool = require("../config/db");

const addActivity = async (req, res) => {
  const { id } = req.id;

  try {
    const { activity, unit, distance, time, createdAt } = req.body;

    const timeline = false;
    const data = await pool.query(
      `INSERT INTO public."Activities" (activity, unit, distance, time, createdBy, timeline, createdat) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *`,
      [activity, unit, distance, time, id, timeline, createdAt]
    );

    res.json({
      success: true,
      message: "Record Saved successfully",
      activity: data.rows[0],
    });
  } catch (error) {
    res.status(500).json({ success: false, message: error.message });
  }
};

const fetchActivity = async (req, res) => {
  const { id } = req.id;
  try {
    const activities = await pool.query(
      `SELECT * FROM public."Activities" WHERE createdby = $1`,
      [id]
    );

    res.json({ success: true, activity: activities.rows });
  } catch (error) {
    res.status(404).json({ success: false, message: "User not Found!" });
  }
};

const updateTimeline = async (req, res) => {
  const { id } = req.params;
  const { timeline } = req.body;

  try {
    await pool.query(
      `UPDATE public."Activities" SET timeline =$1 WHERE id= $2`,
      [timeline, id]
    );
    res.json({
      success: true,
      timeline: timeline,
      message: "Successfully Updated.",
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(400)
      .json({ success: false, message: "Error updating Activity" });
  }
};

const deleteActivity = async (req, res) => {
  const { id } = req.params;
  try {
    await pool.query(`DELETE FROM public."Activities" WHERE id=$1`, [id]);
    res.json({ success: true, message: "Succesfully deleted." });
  } catch (error) {
    console.error(error.message);
    res.status(404).json({ success: false, message: "Error Deleting" });
  }
};

module.exports = { addActivity, fetchActivity, updateTimeline, deleteActivity };
