const pool = require("../config/db");

const userInfo = async (req, res) => {
  const { id } = req.id;

  const user = await pool.query(
    `SELECT firstname, lastname, email, phonenumber,aadhar, bloodgroup,age, weight, id, favorites FROM public."User" WHERE id = $1`,
    [id]
  );

  if (user) {
    res.json({ success: true, userInfo: user.rows[0] });
  } else {
    res.status(404).json({ error: "User not found" });
  }
};

const updateUserInfo = async (req, res) => {
  const { id } = req.id;

  try {
    const {
      firstName,
      lastName,
      email,
      phoneNumber,
      aadhar,
      bloodGroup,
      age,
      weight,
    } = req.body;
    const newData = await pool.query(
      `UPDATE public."User" SET firstname = $1, lastname = $2, email = $3, phonenumber = $4, aadhar = $5, bloodgroup = $6, age = $7, weight = $8 WHERE id = $9 RETURNING *`,
      [
        firstName,
        lastName,
        email,
        phoneNumber,
        aadhar,
        bloodGroup,
        age,
        weight,
        id,
      ]
    );

    res.json({ success: true, userInfo: newData.rows[0] });
  } catch (error) {
    console.error(error.message);
    res.status(409).json({ success: false, message: error.message });
  }
};

const allUsers = async (req, res) => {
  const { id } = req.id;

  const users = await pool.query(
    `SELECT DISTINCT u.id,u.firstname, u.lastname, f.friend, f.senderid, f.receiverid
    FROM "User" u
    JOIN "Activities" a ON u.id = a.createdby
    LEFT JOIN "Friend" f ON (u.id = f.senderid OR u.id = f.receiverid) AND (f.senderid = $1 OR f.receiverid = $1)
    WHERE u.id != $1 AND a.createdby != $1;`,
    [id]
  );

  if (users) {
    res.json({ success: true, users: users.rows });
  } else {
    res.status(404).json({ error: "Users not found" });
  }
};
module.exports = { userInfo, updateUserInfo, allUsers };
