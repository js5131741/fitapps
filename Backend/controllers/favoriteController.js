const pool = require("../config/db");

const updateFavorites = async (req, res) => {
  const { id } = req.id;

  try {
    const { favorites } = req.body;

    await pool.query(`UPDATE public."User" SET favorites =$1 WHERE id= $2`, [
      favorites,
      id,
    ]);
    res.json({ success: true, message: "Successfully Updated." });
  } catch (error) {
    console.error(error.message);
    res
      .status(400)
      .json({ success: false, message: "Error updating Favorites" });
  }
};

module.exports = { updateFavorites };
