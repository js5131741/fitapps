const pool = require("../config/db");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Sign Up
const signUp = async (req, res) => {
  try {
    const { firstName, lastName, password, email, phoneNumber } = req.body;
    const favorites = {
      swimming: false,
      running: false,
      walking: false,
      biking: false,
    };

    const hashedPassword = await bcrypt.hash(password, 10);
    await pool.query(
      `INSERT INTO public."User" (firstname, lastname, password, email, phonenumber, favorites) VALUES ($1, $2, $3, $4, $5, $6)`,
      [firstName, lastName, hashedPassword, email, phoneNumber, favorites]
    );

    res
      .status(201)
      .json({ success: true, message: "User registered successfully" });
  } catch (error) {
    console.error(error.message);
    res.status(400).json({ success: false, message: error.message });
  }
};

// Sign In
const signIn = async (req, res) => {
  try {
    const { email, userPassword } = req.body;
    const jwtSecretKey = process.env.JWT_SECRET_KEY;
    const user = await pool.query(
      `SELECT * FROM public."User" WHERE email = $1 `,
      [email]
    );

    if (user.rows.length === 0) {
      return res
        .status(401)
        .json({ success: false, message: "Invalid credentials" });
    }

    const validPassword = await bcrypt.compare(
      userPassword,
      user.rows[0].password
    );

    if (!validPassword) {
      return res
        .status(401)
        .json({ success: false, message: "Invalid credentials" });
    }

    const token = jwt.sign({ id: user.rows[0].id }, jwtSecretKey);
    const { password, ...userData } = user.rows[0];

    res.status(200).json({
      success: true,
      token: token,
      userInfo: userData,
    });
  } catch (error) {
    console.error(error.message);
    res.status(400).json({ success: false, message: error.message });
  }
};

module.exports = { signUp, signIn };
