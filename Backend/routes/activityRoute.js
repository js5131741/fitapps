const express = require("express");
const activityController = require("../controllers/activityController");
const validateSchema = require("../middlewares/validateSchema");
const { activitySchema, timelineSchema } = require("../validators/activity");
const verifyToken = require("../middlewares/verifyToken");
const router = express.Router();

router.post(
  "/saverecord",
  verifyToken,
  validateSchema(activitySchema),
  activityController.addActivity
);

router.get("/activities", verifyToken, activityController.fetchActivity);

router.patch(
  "/activities/timeline/:id",
  verifyToken,
  validateSchema(timelineSchema),
  activityController.updateTimeline
);

router.delete(
  "/activities/:id",
  verifyToken,
  activityController.deleteActivity
);

module.exports = router;
