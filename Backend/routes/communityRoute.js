const express = require("express");
const validateSchema = require("../middlewares/validateSchema");
const verifyToken = require("../middlewares/verifyToken");
const communityController = require("../controllers/communityController");
const {
  friendSchema,
  acceptRequestSchema,
  removeRequestSchema,
  deleteConnectionSchema,
} = require("../validators/friend");
const router = express.Router();

router.post(
  "/sendrequest",
  validateSchema(friendSchema),
  verifyToken,
  communityController.sendRequest
);
router.delete(
  "/removerequest",
  validateSchema(removeRequestSchema),
  verifyToken,
  communityController.removeRequest
);
router.patch(
  "/acceptrequest",
  validateSchema(acceptRequestSchema),
  verifyToken,
  communityController.acceptRequest
);
router.delete(
  "/removeConnection",
  validateSchema(deleteConnectionSchema),
  verifyToken,
  communityController.removeConnection
);
router.get("/visituser/:userid", verifyToken, communityController.visitUser);

module.exports = router;
