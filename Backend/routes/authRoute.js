const authController = require("../controllers/authController");
const { signUpSchema, singInSchema } = require("../validators/user");
const validateSchema = require("../middlewares/validateSchema");

const express = require("express");
const router = express.Router();

router.post("/signup", validateSchema(signUpSchema), authController.signUp);
router.post("/signin", validateSchema(singInSchema), authController.signIn);

module.exports = router;
