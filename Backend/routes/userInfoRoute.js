const express = require("express");
const verifyToken = require("../middlewares/verifyToken");
const validateSchema = require("../middlewares/validateSchema");
const userInfoController = require("../controllers/userInfoController");
const updateUserSchema = require("../validators/updateUser");
const favoriteController = require("../controllers/favoriteController");
const router = express.Router();

router.get("/userinfo", verifyToken, userInfoController.userInfo);

router.patch(
  "/userinfo",
  verifyToken,
  validateSchema(updateUserSchema),
  userInfoController.updateUserInfo
);

router.patch("/favorites", verifyToken, favoriteController.updateFavorites);

router.get("/allusers", verifyToken, userInfoController.allUsers);

module.exports = router;
