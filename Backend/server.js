const express = require("express");
const { createActivitiesTable } = require("./table/activityTable");
const { createUserTable } = require("./table/userTable");
const { createFriendTable } = require("./table/friendTable");
const cors = require("cors");
require("dotenv").config();

const createTables = async () => {
  try {
    await createUserTable();
    await createActivitiesTable();
    await createFriendTable();
    console.log("Tables created successfully/Already exists.");
  } catch (error) {
    console.error("Error creating Table:", error);
  }
};

const app = express();

createTables();

app.use(cors());
app.use(express.json());

app.use("/", require("./routes/authRoute"));
app.use("/", require("./routes/userInfoRoute"));
app.use("/", require("./routes/activityRoute"));
app.use("/", require("./routes/communityRoute"));

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
