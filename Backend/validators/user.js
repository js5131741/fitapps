const Joi = require("joi");

const signUpSchema = Joi.object({
  firstName: Joi.string().alphanum().required(),

  lastName: Joi.string().alphanum().required(),

  password: Joi.string().required(),

  email: Joi.string().required(),

  phoneNumber: Joi.number().required(),
});

const singInSchema = Joi.object({
  email: Joi.string().required(),
  userPassword: Joi.string().required(),
});

module.exports = { signUpSchema, singInSchema };
