const Joi = require("joi");

const friendSchema = Joi.object({
  receiverId: Joi.number().required(),
  createdAt: Joi.string().required(),
});

const acceptRequestSchema = Joi.object({
  senderid: Joi.number().required(),
});

const removeRequestSchema = Joi.object({
  senderId: Joi.number().required(),
  receiverId: Joi.number().required(),
});

const deleteConnectionSchema = Joi.object({
  id2: Joi.number().required(),
});

module.exports = {
  friendSchema,
  acceptRequestSchema,
  removeRequestSchema,
  deleteConnectionSchema,
};
