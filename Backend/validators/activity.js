const Joi = require("joi");

const activitySchema = Joi.object({
  activity: Joi.string().required(),
  distance: Joi.number().required(),
  unit: Joi.string().required(),
  time: Joi.number().required(),
  createdAt: Joi.string().required(),
});

const timelineSchema = Joi.object({
  timeline: Joi.boolean().required(),
});

module.exports = { activitySchema, timelineSchema };
