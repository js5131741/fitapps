const Joi = require("joi");

const updateUserSchema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().required(),
  phoneNumber: Joi.string().required(),
  aadhar: Joi.string().required(),
  bloodGroup: Joi.string().required(),
  age: Joi.number().integer().required(),
  weight: Joi.number().integer().required(),
});

module.exports = updateUserSchema;
