const pool = require("../config/db");

const createFriendTable = async () => {
  await pool.query(
    `
    CREATE TABLE IF NOT EXISTS public."Friend" (
        id SERIAL PRIMARY KEY,
        senderId INTEGER REFERENCES public."User"(id),
        receiverId INTEGER REFERENCES public."User"(id),
        friend BOOLEAN,
        createdAt VARCHAR(100)
    );    
    `
  );
};

module.exports = {
  createFriendTable,
};
