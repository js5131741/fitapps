const pool = require("../config/db");

const createUserTable = async () => {
  await pool.query(
    `
    CREATE TABLE IF NOT EXISTS public."User" (
        id SERIAL PRIMARY KEY,
        firstname VARCHAR(50),
        lastname VARCHAR(50),
        email VARCHAR(50) UNIQUE,
        phonenumber VARCHAR(10)UNIQUE,
        password VARCHAR(255)UNIQUE,
        aadhar VARCHAR(12),
        bloodgroup VARCHAR(3),
        age INTEGER,
        weight INTEGER,
        favorites JSONB         
    );    
    `
  );
};

module.exports = {
  createUserTable,
};
