const pool = require("../config/db");

const createActivitiesTable = async () => {
  await pool.query(
    `
    CREATE TABLE IF NOT EXISTS public."Activities" (
        id SERIAL PRIMARY KEY,
        activity VARCHAR(20),
        unit VARCHAR(5),
        distance NUMERIC,
        time NUMERIC,
        createdBy INTEGER REFERENCES public."User"(id),
        timeline BOOLEAN,
        createdat VARCHAR(100)
    );    
    `
  );
};

module.exports = {
  createActivitiesTable,
};
