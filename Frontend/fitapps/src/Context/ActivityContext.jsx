import React from "react";

const ActivityContext = React.createContext();

export const ActivityProvider = ({ children }) => {
  const [activity, setActivity] = React.useState();

  return (
    <ActivityContext.Provider value={{ activity, setActivity }}>
      {children}
    </ActivityContext.Provider>
  );
};

export const useActivity = () => React.useContext(ActivityContext);
