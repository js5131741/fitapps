import React from "react";

const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
  const [isLoggedIn, setLoggedIn] = React.useState(false);
  const [userInfo, setUserInfo] = React.useState("");

  const login = () => {
    setLoggedIn(true);
  };
  const logout = () => setLoggedIn(false);

  return (
    <AuthContext.Provider
      value={{ isLoggedIn, login, logout, setUserInfo, userInfo }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => React.useContext(AuthContext);
