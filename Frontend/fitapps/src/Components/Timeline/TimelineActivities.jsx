import React from "react";
import { Card, CardContent, Grid, Typography } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import axios from "axios";
import { useActivity } from "../../Context/ActivityContext";
import { toast } from "react-toastify";
export const TimelineActivities = ({ timelineActivities, deleteIcon }) => {
  const { setActivity } = useActivity();
  const jwtToken = localStorage.getItem("token");
  const formatTime = (seconds) => {
    const minutes = Math.floor(seconds / 60);
    const hours = Math.floor(minutes / 60);

    if (hours > 0) {
      return `${hours} hours`;
    } else if (minutes > 0) {
      return `${minutes} minutes`;
    } else {
      return `${seconds} seconds`;
    }
  };

  const handleDelete = async (id) => {
    try {
      await axios.patch(
        `http://localhost:5000/activities/timeline/${id}`,
        {
          timeline: false,
        },
        {
          headers: {
            Authorization: `${jwtToken}`,
            "Content-Type": "application/json",
          },
        }
      );

      setActivity((prev) =>
        prev.map((item) =>
          item.id === id ? { ...item, timeline: false } : item
        )
      );
      toast.success("Activity removed from Timeline!");
    } catch (error) {
      console.error("Cannot update at the moment", error.message);
    }
  };
  return (
    <div>
      <Grid container spacing={5} columns={4}>
        {timelineActivities?.map((data, index) => {
          return (
            <Grid item key={index}>
              <Card
                sx={{
                  m: 0,
                  p: 0,
                  "&:hover": {
                    transform: "scale(1.05)",
                  },
                }}
              >
                <CardContent
                  sx={{
                    p: 0,
                  }}
                >
                  <div
                    style={{
                      backgroundImage: `url(/images/${data.activity}.jpg)`,
                      height: "175px",
                      width: "350px",
                      backgroundPosition: "center center",
                      backgroundRepeat: "no-repeat",
                      backgroundSize: "cover",
                    }}
                  ></div>
                  <div style={{ padding: "24px", paddingBottom: 0 }}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <Typography>
                        <b>Activity: </b> {data.activity.toUpperCase()}
                      </Typography>
                      {deleteIcon && (
                        <DeleteIcon
                          onClick={() => handleDelete(data.id)}
                          sx={{ color: "red", cursor: "pointer" }}
                        />
                      )}
                    </div>

                    <Typography>
                      <b>Distance : </b>
                      {`${data.distance} ${data.unit}`}
                    </Typography>

                    <Typography>
                      <b>Time : </b> {formatTime(data.time)}
                    </Typography>

                    <Typography>
                      <b>Date : </b> {data.createdat}
                    </Typography>
                  </div>
                </CardContent>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
};
