import React from "react";
import { useAuth } from "../../Context/AuthContext";

import { IncompleteUserInfo } from "../Messages/IncompleteUserInfo";
import { Typography } from "@mui/material";
import { useActivity } from "../../Context/ActivityContext";

import axios from "axios";
import { toast } from "react-toastify";
import { useParams } from "react-router-dom";
import { TimelineActivities } from "./TimelineActivities";
export const Timeline = () => {
  let { id } = useParams();
  id = parseInt(id, 10);
  const { userInfo } = useAuth();
  const { activity } = useActivity();

  let isComplete = true;
  const [timelineActivities, setTimelineActivities] = React.useState([]);

  const jwtToken = localStorage.getItem("token");

  for (const key in userInfo) {
    if (userInfo[key] === null && key !== "favorites") {
      isComplete = false;
      break;
    }
  }

  React.useEffect(() => {
    const timelineActivitiesfunc = async () => {
      if (parseInt(id, 10) === userInfo.id) {
        const filteredTimelineActivities =
          activity?.filter((item) => item.timeline === true) || [];
        setTimelineActivities(filteredTimelineActivities);
      } else {
        try {
          const response = await axios.get(
            `http://localhost:5000/visituser/${id}`,
            {
              headers: {
                Authorization: `${jwtToken}`,
                "Content-Type": "application/json",
              },
            }
          );

          setTimelineActivities(response.data.activity);
        } catch (error) {
          toast.error("Cannot fetch User Data!");
          console.error(error.message);
        }
      }
    };
    timelineActivitiesfunc();
    // eslint-disable-next-line
  }, [activity, id]);

  return (
    <div className="features-main">
      {!isComplete ? (
        <div>
          <Typography variant="h4" sx={{ mb: 2 }}>
            My TimeLine
          </Typography>
          <IncompleteUserInfo />
        </div>
      ) : (id === userInfo.id) & (timelineActivities?.length === 0) ? (
        <Typography variant="h5">
          Add Activities to your Timeline from the Performance tab.
        </Typography>
      ) : (id !== userInfo.id) & (timelineActivities?.length === 0) ? (
        <Typography variant="h5">No Activities on User's timeline.</Typography>
      ) : id === userInfo.id ? (
        <div>
          <Typography variant="h4" sx={{ mb: 2 }}>
            My TimeLine
          </Typography>
          <TimelineActivities
            timelineActivities={timelineActivities}
            deleteIcon={true}
          />
        </div>
      ) : (
        id !== userInfo.id && (
          <div>
            <Typography variant="h4" sx={{ mb: 2 }}>
              {`${
                timelineActivities[0]?.firstname +
                " " +
                timelineActivities[0]?.lastname
              }'s TimeLine`}
            </Typography>
            <TimelineActivities timelineActivities={timelineActivities} />
          </div>
        )
      )}
    </div>
  );
};
