import { Typography } from "@mui/material";
import React from "react";
import { useAuth } from "../../Context/AuthContext";

export const HomePage = () => {
  const { isLoggedIn } = useAuth();
  return (
    <div>
      {!isLoggedIn ? (
        <Typography
          variant="h4"
          sx={{
            textAlign: "center",
            position: "absolute",
            top: "40%",
            left: "40%",
          }}
        >
          SignUp/SignIn to explore
        </Typography>
      ) : (
        <Typography
          variant="h4"
          sx={{
            textAlign: "center",
            position: "absolute",
            top: "40%",
            left: "40%",
          }}
        >
          HomePage
        </Typography>
      )}
    </div>
  );
};
