import { Routes, Route } from "react-router-dom";
import SignUp from "./SignUpLogin/SignUp";
import SignIn from "./SignUpLogin/SignIn";
import { Profile } from "./UserProfile/Profile";
import { Header } from "./Header";
import { useAuth } from "../Context/AuthContext";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Timeline } from "./Timeline/Timeline";
import { Performance } from "./Performance";
import { Activities } from "./Activities/Activities";
import React from "react";
import axios from "axios";
import { useActivity } from "../Context/ActivityContext";
import moment from "moment";
import { Community } from "./Community/Community";
import { HomePage } from "./HomePage/HomePage";

function App() {
  const { isLoggedIn, login, setUserInfo } = useAuth();
  const { setActivity } = useActivity();
  const jwtToken = localStorage.getItem("token");

  if (jwtToken) {
    login();
  }
  React.useEffect(() => {
    const fetchData = async () => {
      try {
        const [userInfoResponse, activityResponse] = await Promise.all([
          axios.get(`http://localhost:5000/userinfo`, {
            headers: {
              Authorization: jwtToken,
              "Content-Type": "application/json",
            },
          }),
          axios.get(`http://localhost:5000/activities`, {
            headers: {
              Authorization: jwtToken,
              "Content-Type": "application/json",
            },
          }),
        ]);

        setUserInfo(userInfoResponse.data.userInfo);
        setActivity(
          activityResponse.data.activity.sort((a, b) => {
            const dateA = moment(a.createdat, "MMMM Do YYYY, h:mm:ss a");
            const dateB = moment(b.createdat, "MMMM Do YYYY, h:mm:ss a");
            return dateB.diff(dateA);
          })
        );

        if (
          userInfoResponse.status === 200 &&
          activityResponse.status === 200
        ) {
          login();
        }
      } catch (error) {
        console.error(
          "Error fetching user information or activity data:",
          error.message
        );
        toast("There was an issue retrieving your data");
      }
    };
    if (isLoggedIn) {
      fetchData();
    }
    // eslint-disable-next-line
  }, [isLoggedIn]);

  return (
    <div className="App">
      <Header />

      <Routes>
        <Route path="/" element={<HomePage />}></Route>
        <Route path="/signup" element={<SignUp />}></Route>
        <Route path="/signin" element={<SignIn />}></Route>
        <Route path="/profile" element={<Profile />}></Route>
        <Route path="/timeline/:id" element={<Timeline />}></Route>
        <Route path="/activities" element={<Activities />}></Route>
        <Route path="/performance" element={<Performance />}></Route>
        <Route path="/community" element={<Community />}></Route>
      </Routes>

      <ToastContainer />
    </div>
  );
}

export default App;
