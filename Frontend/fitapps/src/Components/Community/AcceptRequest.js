import axios from "axios";
import { toast } from "react-toastify";

export async function acceptRequest(id, setAllUsers, jwtToken) {
  try {
    await axios.patch(
      `http://localhost:5000/acceptrequest`,
      {
        senderid: id,
      },
      {
        headers: {
          Authorization: `${jwtToken}`,
          "Content-Type": "application/json",
        },
      }
    );

    setAllUsers((prev) =>
      prev.map((user) => {
        if (user.id === id) {
          return { ...user, friend: true };
        } else {
          return user;
        }
      })
    );
    toast.success("Request Accepted!");
  } catch (error) {
    toast.error("Cannot Accept!");
    console.error(error.message);
  }
}
