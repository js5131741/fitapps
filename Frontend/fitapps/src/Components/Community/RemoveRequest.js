import axios from "axios";
import { toast } from "react-toastify";

export async function removeRequest(
  senderid,
  receiverid,
  setAllUsers,
  jwtToken
) {
  try {
    const response = await axios.delete(`http://localhost:5000/removerequest`, {
      headers: {
        Authorization: `${jwtToken}`,
        "Content-Type": "application/json",
      },
      data: {
        senderId: senderid,
        receiverId: receiverid,
      },
    });
    if (response.data.success) {
      setAllUsers((prev) =>
        prev.map((user) => {
          if ((user.senderid === senderid) & (user.receiverid === receiverid)) {
            return {
              ...user,
              friend: null,
              senderid: null,
              receiverid: null,
            };
          } else {
            return user;
          }
        })
      );
      toast.success("Removed Request!");
    }
  } catch (error) {
    console.error(error);
    toast.error("Error removing Request!");
  }
}
