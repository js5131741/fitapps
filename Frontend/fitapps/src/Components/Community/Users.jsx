import { Button, Card, CardContent, Typography } from "@mui/material";
import React from "react";
import ProfileImage from "../UserProfile/ProfileImage";
import { toast } from "react-toastify";
import moment from "moment";
import axios from "axios";
import { useAuth } from "../../Context/AuthContext";
import { acceptRequest } from "./AcceptRequest";
import { removeRequest } from "./RemoveRequest";

export const Users = ({ allUsers, setAllUsers }) => {
  const { userInfo } = useAuth();
  const jwtToken = localStorage.getItem("token");
  const filteredUsers = allUsers.filter((item) => item.friend !== true);

  const isReceived = allUsers.map((item) => item.senderid);

  const handleRequest = async (id) => {
    try {
      const response = await axios.post(
        `http://localhost:5000/sendrequest`,
        {
          receiverId: id,
          createdAt: moment().format("MMMM Do YYYY, h:mm:ss a"),
        },
        {
          headers: {
            Authorization: `${jwtToken}`,
            "Content-Type": "application/json",
          },
        }
      );
      if (response.data.success) {
        setAllUsers((prev) =>
          prev.map((user) => {
            if (user.id === id) {
              return {
                ...user,
                senderid: userInfo.id,
                receiverid: id,
                friend: false,
              };
            } else {
              return user;
            }
          })
        );
      }
      toast.success("Request sent!");
    } catch (error) {
      console.error(error);
      toast.error("Error sending Request!");
    }
  };

  const handleRemove = async (id) => {
    const senderid = userInfo.id;
    const receiverid = id;
    removeRequest(senderid, receiverid, setAllUsers, jwtToken);
  };

  const handleAccept = async (id) => {
    await acceptRequest(id, setAllUsers, jwtToken);
  };

  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
      className="community-tab-body"
    >
      {filteredUsers.map((item) => (
        <Card
          key={item.id}
          id={item.id}
          sx={{ mb: 2, width: "50%", padding: "0 30px" }}
        >
          <CardContent
            sx={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div style={{ display: "flex", alignItems: "center", gap: "2rem" }}>
              <ProfileImage name={item.firstname + " " + item.lastname} />
              <Typography>{item.firstname + " " + item.lastname}</Typography>
            </div>
            <div style={{ display: "flex", alignItems: "center", gap: "2rem" }}>
              {isReceived.includes(item.id) ? (
                <Button
                  variant="contained"
                  onClick={() => handleAccept(item.id)}
                  sx={{
                    backgroundColor: "green",
                    cursor: "default",
                    "&:hover": {
                      backgroundColor: "green",
                      transform: "scale(1.1)",
                    },
                  }}
                >
                  Accept Request
                </Button>
              ) : item.friend === null ? (
                <Button
                  variant="contained"
                  sx={{
                    backgroundColor: "green",
                    "&:hover": {
                      backgroundColor: "green",
                      transform: "scale(1.1)",
                    },
                  }}
                  onClick={() => handleRequest(item.id)}
                >
                  Request Connection
                </Button>
              ) : (
                <div style={{ display: "flex", gap: "2rem" }}>
                  <Button
                    variant="contained"
                    sx={{
                      backgroundColor: "grey",
                      cursor: "default",
                      "&:hover": {
                        backgroundColor: "grey",
                      },
                    }}
                  >
                    Request Sent!
                  </Button>
                  <Button
                    variant="contained"
                    onClick={() => handleRemove(item.id)}
                    sx={{
                      backgroundColor: "red",
                      "&:hover": {
                        backgroundColor: "red",
                        transform: "scale(1.1)",
                      },
                    }}
                  >
                    Remove Request
                  </Button>
                </div>
              )}
            </div>
          </CardContent>
        </Card>
      ))}
    </div>
  );
};
