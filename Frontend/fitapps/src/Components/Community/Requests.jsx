import React from "react";
import { acceptRequest } from "./AcceptRequest";
import { useAuth } from "../../Context/AuthContext";
import { Button, Card, CardContent, Typography } from "@mui/material";
import ProfileImage from "../UserProfile/ProfileImage";
import { removeRequest } from "./RemoveRequest";

export const Requests = ({ allUsers, setAllUsers }) => {
  const { userInfo } = useAuth();
  const jwtToken = localStorage.getItem("token");

  const requests = allUsers.map((item) => {
    if (item.receiverid === userInfo.id) {
      return item.senderid;
    } else {
      return 0;
    }
  });

  const requestsData = allUsers.filter(
    (item) => requests.includes(item.id) & (item.friend !== true)
  );

  const handleAccept = (id) => {
    acceptRequest(id, setAllUsers, jwtToken);
  };

  const handleRemove = async (id) => {
    const senderid = id;
    const receiverid = userInfo.id;
    removeRequest(senderid, receiverid, setAllUsers, jwtToken);
  };
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
      className="community-tab-body"
    >
      {requestsData.map((item) => (
        <Card
          key={item.id}
          id={item.id}
          sx={{ mb: 2, width: "50%", padding: "0 30px" }}
        >
          <CardContent
            sx={{
              display: "flex",
              justifyContent: "space-between",
            }}
          >
            <div style={{ display: "flex", alignItems: "center", gap: "2rem" }}>
              <ProfileImage name={item.firstname + " " + item.lastname} />
              <Typography>{item.firstname + " " + item.lastname}</Typography>
            </div>
            <div style={{ display: "flex", alignItems: "center", gap: "2rem" }}>
              <Button
                variant="contained"
                onClick={() => handleAccept(item.id)}
                sx={{
                  backgroundColor: "green",
                  cursor: "default",
                  "&:hover": {
                    backgroundColor: "green",
                    transform: "scale(1.1)",
                  },
                }}
              >
                Accept
              </Button>

              <Button
                variant="contained"
                onClick={() => handleRemove(item.id)}
                sx={{
                  backgroundColor: "red",
                  "&:hover": {
                    backgroundColor: "red",
                    transform: "scale(1.1)",
                  },
                }}
              >
                Remove
              </Button>
            </div>
          </CardContent>
        </Card>
      ))}
    </div>
  );
};
