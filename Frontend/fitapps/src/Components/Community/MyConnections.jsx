import React from "react";
import { IncompleteUserInfo } from "../Messages/IncompleteUserInfo";
import { Button, Card, CardContent, Typography } from "@mui/material";
import ProfileImage from "../UserProfile/ProfileImage";
import axios from "axios";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

export const MyConnections = ({ isComplete, allUsers, setAllUsers }) => {
  const friendData = allUsers.filter((item) => item.friend);
  const jwtToken = localStorage.getItem("token");
  // const [pageNumber, setPageNumber] = React.useState(1);
  const navigate = useNavigate();
  const handleRemove = async (id) => {
    try {
      const response = await axios.delete(
        `http://localhost:5000/removeconnection`,
        {
          headers: {
            Authorization: `${jwtToken}`,
            "Content-Type": "application/json",
          },
          data: {
            id2: id,
          },
        }
      );
      if (response.data.success) {
        toast.success("Connection Removed!");
        setAllUsers((prev) =>
          prev.map((item) =>
            item.id === id
              ? { ...item, friend: null, senderid: null, receiverd: null }
              : item
          )
        );
      }
    } catch (error) {
      toast.error("Cannot update at the moment");
      console.error("Cannot update at the moment", error.message);
    }
  };

  const handleVisit = (id) => {
    navigate(`/timeline/${id}`);
  };
  return (
    <div>
      {!isComplete ? (
        <div>
          <IncompleteUserInfo />
        </div>
      ) : (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
          }}
          className="community-tab-body"
        >
          {friendData.map((item) => (
            <Card
              key={item.id}
              id={item.id}
              sx={{ mb: 2, width: "50%", padding: "0 30px" }}
            >
              <CardContent
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    gap: "1rem",
                    cursor: "pointer",
                  }}
                  onClick={() => handleVisit(item.id)}
                >
                  <ProfileImage name={item.firstname + " " + item.lastname} />
                  <Typography>
                    {item.firstname + " " + item.lastname}
                  </Typography>
                </div>
                <Button
                  variant="outlined"
                  onClick={() => handleRemove(item.id)}
                  sx={{
                    color: "red",
                    border: "1px solid black",
                    "&:hover": {
                      backgroundColor: "red",
                      color: "white",
                      border: "none",
                      transform: "scale(1.05)",
                    },
                  }}
                >
                  Remove Connection
                </Button>
              </CardContent>
            </Card>
          ))}
        </div>
      )}
    </div>
  );
};
