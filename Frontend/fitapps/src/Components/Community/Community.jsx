import React from "react";
import { MyConnections } from "./MyConnections";
import { Users } from "./Users";
import { Requests } from "./Requests";
import { useAuth } from "../../Context/AuthContext";
import axios from "axios";
import PropTypes from "prop-types";
import { toast } from "react-toastify";
import { Box, Tab, Tabs } from "@mui/material";

export const Community = () => {
  const { userInfo } = useAuth();
  const [allUsers, setAllUsers] = React.useState([]);
  const [value, setValue] = React.useState(0);
  const requestsNumber = allUsers.filter(
    (item) => (item.receiverid === userInfo.id) & (item.friend === false)
  ).length;

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const jwtToken = localStorage.getItem("token");
  let isComplete = true;
  for (const key in userInfo) {
    if (userInfo[key] === null && key !== "favorites") {
      isComplete = false;
      break;
    }
  }

  function CustomTabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
      </div>
    );
  }

  CustomTabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };

  React.useEffect(() => {
    const fetchCommunityData = async () => {
      try {
        const allUsersResponse = await axios.get(
          `http://localhost:5000/allusers`,
          {
            headers: {
              Authorization: jwtToken,
              "Content-Type": "application/json",
            },
          }
        );

        setAllUsers(allUsersResponse.data.users);
      } catch (error) {
        console.error(
          "Error fetching user information or activity data:",
          error.message
        );
        toast("There was an issue retrieving your data");
      }
    };

    fetchCommunityData();
    //eslint-disable-next-line
  }, []);

  return (
    <Box sx={{ width: "100%" }}>
      <Box
        sx={{
          borderBottom: 1,
          borderColor: "divider",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <Tabs value={value} onChange={handleChange} aria-label="tabs">
          <Tab label="Users" id="tab-1" aria-controls="tabpanel-1" />
          <Tab label="My Connections" id="tab-2" aria-controls="tabpanel-2" />
          <Tab
            label={
              <div
                style={{ display: "flex", gap: "6px", alignItems: "center" }}
              >
                Requests
                {requestsNumber > 0 && (
                  <div className="counter">{requestsNumber}</div>
                )}
              </div>
            }
            id="tab-3"
            aria-controls="tabpanel-3"
          />
        </Tabs>
      </Box>
      <CustomTabPanel value={value} index={0}>
        <Users allUsers={allUsers} setAllUsers={setAllUsers} />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={1}>
        <MyConnections
          isComplete={isComplete}
          allUsers={allUsers}
          setAllUsers={setAllUsers}
        />
      </CustomTabPanel>
      <CustomTabPanel value={value} index={2}>
        <Requests allUsers={allUsers} setAllUsers={setAllUsers} />
      </CustomTabPanel>
    </Box>
  );
};
