import React from "react";
import { useAuth } from "../../Context/AuthContext";
import { Typography } from "@mui/material";
import { Link } from "react-router-dom";

export const IncompleteUserInfo = () => {
  const { userInfo } = useAuth();
  const incompleteInfo = [];

  for (const key in userInfo) {
    if (userInfo[key] === null && key !== "favorites") {
      incompleteInfo.push(key.toUpperCase());
    }
  }

  return (
    <div>
      {incompleteInfo.length > 0 && (
        <Link to="/profile">
          <div style={{ textAlign: "center", cursor: "pointer" }}>
            <Typography
              variant="h5"
              sx={{ color: "red" }}
            >{`${incompleteInfo.join(", ")} data are missing!`}</Typography>
            <Typography variant="h5" sx={{ color: "red" }}>
              Please fill them to gain access.
            </Typography>
          </div>
        </Link>
      )}
    </div>
  );
};
