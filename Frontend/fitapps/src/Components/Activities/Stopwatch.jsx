import { Button } from "@mui/material";
import axios from "axios";
import React from "react";
import { toast } from "react-toastify";
import { useActivity } from "../../Context/ActivityContext";
import moment from "moment";

const Stopwatch = ({ measurement }) => {
  const [time, setTime] = React.useState(0);
  const [isPaused, setIsPaused] = React.useState(false);

  const jwtToken = localStorage.getItem("token");
  const [isRunning, setIsRunning] = React.useState(false);
  const { setActivity } = useActivity();

  const saveRecord = async () => {
    try {
      const response = await axios.post(
        `http://localhost:5000/saverecord`,
        {
          activity: measurement.activity,
          unit: measurement.unit,
          distance: measurement.distance,
          time: time,
          createdAt: moment().format("MMMM Do YYYY, h:mm:ss a"),
        },
        {
          headers: {
            Authorization: `${jwtToken}`,
            "Content-Type": "application/json",
          },
        }
      );
      if (response.data.success) {
        toast.success("Record Saved!");
        setTime(0);
        setIsRunning(false);
        setIsPaused(false);
        setActivity((prev) => [response.data.activity, ...prev]);
      }
    } catch (error) {
      toast.error("Save Failed! Try again.");
      console.error("Cannot Add.", error.message);
    }
  };

  React.useEffect(() => {
    let intervalId;
    if (isRunning) {
      intervalId = setInterval(() => setTime(time + 1), 1000);
    }
    return () => clearInterval(intervalId);
  }, [isRunning, time]);

  const hours = Math.floor(time / 3600);
  const minutes = Math.floor((time % 3600) / 60);
  const seconds = Math.floor(time % 60);

  const startAndStop = () => {
    setIsRunning(!isRunning);
    setIsPaused(true);
  };

  const reset = () => {
    setTime(0);
    setIsRunning(false);
    setIsPaused(false);
  };
  return (
    <div className="stopwatch-container">
      <p className="stopwatch-time">
        {hours}:{minutes.toString().padStart(2, "0")}:
        {seconds.toString().padStart(2, "0")}
      </p>
      <div className="stopwatch-buttons">
        {isRunning ? (
          <Button
            className="stopwatch-button"
            style={{ backgroundColor: "red" }}
            onClick={startAndStop}
          >
            Pause
          </Button>
        ) : isPaused ? (
          <div>
            <Button
              className="stopwatch-button"
              style={{ backgroundColor: "green" }}
              onClick={startAndStop}
            >
              Resume
            </Button>
            <Button
              className="stopwatch-button"
              style={{ backgroundColor: "green" }}
              onClick={saveRecord}
            >
              Save Record
            </Button>
            <Button
              className="stopwatch-button"
              style={{ backgroundColor: "red" }}
              onClick={reset}
            >
              Reset
            </Button>
          </div>
        ) : (
          <Button
            className="stopwatch-button"
            style={{ backgroundColor: "green" }}
            onClick={startAndStop}
          >
            Start
          </Button>
        )}
      </div>
    </div>
  );
};

export default Stopwatch;
