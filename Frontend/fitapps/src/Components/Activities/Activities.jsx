import React from "react";
import { useAuth } from "../../Context/AuthContext";
import { IncompleteUserInfo } from "../Messages/IncompleteUserInfo";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import PoolIcon from "@mui/icons-material/Pool";
import DirectionsRunIcon from "@mui/icons-material/DirectionsRun";
import DirectionsWalkIcon from "@mui/icons-material/DirectionsWalk";
import DirectionsBikeIcon from "@mui/icons-material/DirectionsBike";
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import axios from "axios";
import { toast } from "react-toastify";
import Stopwatch from "./Stopwatch";

export const Activities = () => {
  const { userInfo, setUserInfo } = useAuth();
  const jwtToken = localStorage.getItem("token");
  const distance = {
    mts: [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000],
    kms: [0.5, 1, 2, 3, 4, 5, 10, 15, 20, 30],
  };
  const [currentFavorites, setCurrentFavorites] = React.useState();
  const [measurement, setMeasurement] = React.useState({
    activity: "",
    unit: "",
    distance: "",
  });
  const [isDetails, setIsDetails] = React.useState(false);

  const checkDetails = () => {
    let flag = Object.values(measurement).every((value) => Boolean(value));

    if (flag) {
      setIsDetails(true);
    }
  };

  const handleMeasurement = (e, key) => {
    if (key === "activity") {
      setMeasurement({
        activity: e.target.value,
        unit: "",
        distance: "",
      });
      setIsDetails(false);
    } else if (key === "unit") {
      setMeasurement((prev) => ({
        ...prev,
        [key]: e.target.value,
        distance: "",
      }));
      setIsDetails(false);
    } else {
      setMeasurement((prev) => ({
        ...prev,
        distance: e.target.value,
      }));
    }
  };

  let isComplete = true;
  for (const key in userInfo) {
    if (userInfo[key] === null && key !== "favorites") {
      isComplete = false;
      break;
    }
  }

  const handleCheckboxChange = async (value) => {
    const favorites = {
      ...currentFavorites,
      [value]: !currentFavorites[value],
    };
    setCurrentFavorites(favorites);
    try {
      const response = await axios.patch(
        `http://localhost:5000/favorites`,
        {
          favorites: favorites,
        },
        {
          headers: {
            Authorization: `${jwtToken}`,
            "Content-Type": "application/json",
          },
        }
      );
      if (response.status === 200) {
        setUserInfo({ ...userInfo, favorites: favorites });
      } else {
        console.error("Failed to fetch user information");
        toast.error("Cannot Save changes at the moment!");
      }
    } catch (error) {
      console.error("Cannot Save changes at the moment", error.message);
    }
  };

  React.useEffect(() => {
    setCurrentFavorites(userInfo.favorites);
    checkDetails();
    //eslint-disable-next-line
  }, [userInfo, measurement]);

  return (
    <div className="features-main">
      {!isComplete ? (
        <div>
          <Typography variant="h4" sx={{ mb: 2 }}>
            Record Activities:
          </Typography>
          <IncompleteUserInfo />
        </div>
      ) : (
        currentFavorites && (
          <div>
            <Accordion sx={{ width: "30vw", mb: 3 }}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography>Select Favorites</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={currentFavorites?.swimming}
                        onChange={() => handleCheckboxChange("swimming")}
                      />
                    }
                    label={
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <PoolIcon sx={{ mr: 1 }} />
                        Swimming
                      </div>
                    }
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={currentFavorites?.running}
                        onChange={() => handleCheckboxChange("running")}
                      />
                    }
                    label={
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <DirectionsRunIcon sx={{ mr: 1 }} />
                        Running
                      </div>
                    }
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={currentFavorites?.walking}
                        onChange={() => handleCheckboxChange("walking")}
                      />
                    }
                    label={
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <DirectionsWalkIcon sx={{ mr: 1 }} />
                        Walking
                      </div>
                    }
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={currentFavorites?.biking}
                        onChange={() => handleCheckboxChange("biking")}
                      />
                    }
                    label={
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <DirectionsBikeIcon sx={{ mr: 1 }} />
                        Biking
                      </div>
                    }
                  />
                </FormGroup>
              </AccordionDetails>
            </Accordion>
            <Accordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography>Measure Performance</Typography>
              </AccordionSummary>
              <AccordionDetails sx={{ display: "flex" }}>
                <FormControl>
                  <InputLabel>Activity</InputLabel>
                  <Select
                    className="measure-activity"
                    value={measurement.activity}
                    onChange={(e) => handleMeasurement(e, "activity")}
                    sx={{ width: "20vh" }}
                    label="Activity"
                  >
                    {currentFavorites &&
                      Object.keys(currentFavorites).map((key) => {
                        if (currentFavorites[key] === true) {
                          return (
                            <MenuItem key={key} value={key}>
                              {key.toUpperCase()}
                            </MenuItem>
                          );
                        }
                        return null;
                      })}
                  </Select>
                </FormControl>

                <FormControl>
                  <InputLabel>Unit</InputLabel>
                  <Select
                    className="measure-activity"
                    value={measurement.unit}
                    onChange={(e) => handleMeasurement(e, "unit")}
                    sx={{ width: "20vh" }}
                    label="Unit"
                  >
                    {measurement.activity === "swimming" ? (
                      <MenuItem key="mts" value="Mts">
                        Mts
                      </MenuItem>
                    ) : (
                      <MenuItem key="kms" value="Kms">
                        Kms
                      </MenuItem>
                    )}
                  </Select>
                </FormControl>

                <FormControl>
                  <InputLabel>Distance</InputLabel>
                  <Select
                    className="measure-activity"
                    value={measurement.distance}
                    onChange={(e) => handleMeasurement(e, "distnace")}
                    sx={{ width: "20vh" }}
                    label="Distance"
                  >
                    {measurement.unit === "Mts"
                      ? distance.mts.map((item) => (
                          <MenuItem key={item} value={item}>
                            {item}
                          </MenuItem>
                        ))
                      : distance.kms.map((item) => (
                          <MenuItem key={item} value={item}>
                            {item}
                          </MenuItem>
                        ))}
                  </Select>
                </FormControl>
              </AccordionDetails>

              <AccordionDetails>
                {isDetails && <Stopwatch measurement={measurement} />}
              </AccordionDetails>
            </Accordion>
          </div>
        )
      )}
    </div>
  );
};
