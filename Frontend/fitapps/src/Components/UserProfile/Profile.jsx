import { Box, TextField, Typography, Container, Button } from "@mui/material";
import React from "react";
import axios from "axios";
import { toast } from "react-toastify";
import { useAuth } from "../../Context/AuthContext";
import validator from "validator";

export const Profile = () => {
  const jwtToken = localStorage.getItem("token");
  const [isError, setIsError] = React.useState({ error: false, column: "" });
  const [formError, setFormError] = React.useState({
    phoneNumber: false,
    aadhar: false,
    bloodGroup: false,
    email: false,
  });

  const { userInfo, setUserInfo } = useAuth();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    try {
      const obj = {
        phoneNumber: data.get("number").toString().length !== 10 ? true : false,
        aadhar: data.get("aadhar").toString().length !== 12 ? true : false,
        bloodGroup: data.get("bloodGroup").match(/^(A|B|AB|O)[+-]$/)
          ? false
          : true,
        email: validator.isEmail(data.get("email")) ? false : true,
      };
      setFormError(obj);
      if (Object.values(obj).includes(true)) {
        throw new Error("Error");
      }
    } catch (error) {
      return;
    }

    try {
      const response = await axios.patch(
        `http://localhost:5000/userInfo`,
        {
          firstName: data.get("firstName"),
          lastName: data.get("lastName"),
          phoneNumber: data.get("number"),
          email: data.get("email"),
          aadhar: data.get("aadhar"),
          bloodGroup: data.get("bloodGroup"),
          age: data.get("age"),
          weight: data.get("weight"),
        },
        {
          headers: {
            Authorization: `${jwtToken}`,
            "Content-Type": "application/json",
          },
        }
      );
      if (response.status === 200) {
        setUserInfo(response.data.userInfo);
        setIsError(false);
        toast.success("Changes Saved!");
      } else {
        console.error("Failed to fetch user information");
        toast.error("Cannot Save changes at the moment!");
      }
    } catch (error) {
      console.error("Cannot Save changes at the moment", error.message);
      const field = error.response.data.message.match(/"([^"]+)"/);
      setIsError({
        error: true,
        column: field ? field[1].toUpperCase() : null,
      });
    }
  };

  return (
    <div>
      {userInfo && (
        <Container component="main" maxWidth="sm">
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
              <Typography variant="h5">Profile Information</Typography>

              <TextField
                required
                name="firstName"
                label="First Name"
                variant="outlined"
                margin="normal"
                fullWidth
                defaultValue={userInfo.firstname}
              />
              <TextField
                required
                name="lastName"
                label="Last Name"
                variant="outlined"
                margin="normal"
                fullWidth
                defaultValue={userInfo.lastname}
              />
              <TextField
                required
                name="email"
                label="Email"
                type="email"
                variant="outlined"
                margin="normal"
                fullWidth
                defaultValue={userInfo.email}
                error={formError.email}
                helperText={formError.email ? "Please enter a valid email" : ""}
              />
              <TextField
                required
                name="number"
                label="Phone Number"
                variant="outlined"
                margin="normal"
                type="number"
                fullWidth
                defaultValue={userInfo.phonenumber}
                error={formError.phoneNumber}
                helperText={
                  formError.phoneNumber ? "Phone number must be 10 digits" : ""
                }
              />
              <TextField
                required
                name="aadhar"
                label="Aadhar"
                variant="outlined"
                margin="normal"
                type="number"
                fullWidth
                defaultValue={userInfo.aadhar}
                error={formError.aadhar}
                helperText={formError.aadhar ? "Aadhar must be 12 digits" : ""}
              />
              <TextField
                required
                name="bloodGroup"
                label="Blood Group"
                variant="outlined"
                margin="normal"
                fullWidth
                defaultValue={userInfo.bloodgroup}
                error={formError.bloodGroup}
                helperText={
                  formError.bloodGroup ? "Please enter valid BloodGroup" : ""
                }
              />
              <TextField
                required
                name="age"
                label="Age"
                variant="outlined"
                margin="normal"
                type="number"
                fullWidth
                defaultValue={userInfo.age}
                InputProps={{ inputProps: { min: 0, max: 100 } }}
              />

              <TextField
                required
                name="weight"
                label="Weight"
                variant="outlined"
                margin="normal"
                type="number"
                fullWidth
                defaultValue={userInfo.weight}
              />
              {isError.error && (
                <Typography sx={{ fontSize: "1rem", color: "red", mt: 1 }}>
                  {isError.column} belongs to another user!
                </Typography>
              )}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2, fontSize: "1.25rem" }}
              >
                Save Changes
              </Button>
            </Box>
          </Box>
        </Container>
      )}
    </div>
  );
};
