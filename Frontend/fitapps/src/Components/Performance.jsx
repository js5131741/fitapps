import React from "react";
import { useAuth } from "../Context/AuthContext";
import { IncompleteUserInfo } from "./Messages/IncompleteUserInfo";
import { useActivity } from "../Context/ActivityContext";
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  Menu,
  MenuItem,
  Radio,
  Typography,
} from "@mui/material";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Toolbar from "@mui/material/Toolbar";
import Paper from "@mui/material/Paper";

import DeleteIcon from "@mui/icons-material/Delete";
import FilterListIcon from "@mui/icons-material/FilterList";
import { visuallyHidden } from "@mui/utils";
import { Button } from "@mui/material";
import axios from "axios";
import { toast } from "react-toastify";

export const Performance = () => {
  const { userInfo } = useAuth();
  const { activity, setActivity } = useActivity();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [filtered, setFiltered] = React.useState({
    activity: "none",
    topfive: false,
  });

  const [filterActivities, setFilterActivities] = React.useState([]);
  let isComplete = true;
  const jwtToken = localStorage.getItem("token");
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  for (const key in userInfo) {
    if (userInfo[key] === null && key !== "favorites") {
      isComplete = false;
      break;
    }
  }

  function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  function getComparator(order, orderBy) {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  const handleRadioChange = (e) => {
    setFiltered((prev) => ({ ...prev, activity: e.target.value }));
  };

  const headCells = [
    {
      id: "activity",
      numeric: false,
      disablePadding: true,
      label: "Activity",
    },
    {
      id: "distance",
      numeric: true,
      disablePadding: false,
      label: "Distance",
    },
    {
      id: "unit",
      numeric: false,
      disablePadding: false,
      label: "Unit",
    },
    {
      id: "time",
      numeric: true,
      disablePadding: false,
      label: "Time(s)",
    },
    {
      id: "createdat",
      numeric: false,
      disablePadding: false,
      label: "Date",
    },
    {
      id: "timeline",
      numeric: false,
      disablePadding: false,
      label: "Timeline",
    },
    {
      id: "delete",
      numeric: false,
      disablePadding: false,
      label: "Delete",
    },
  ];

  function EnhancedTableHead(props) {
    const { order, orderBy, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align="center"
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : "asc"}
                onClick={
                  headCell.id === "timeline" || headCell.id === "delete"
                    ? null
                    : createSortHandler(headCell.id)
                }
              >
                <Typography variant="h5">{headCell.label}</Typography>
                {orderBy === headCell.id ? (
                  <Box component="span" sx={visuallyHidden}>
                    {order === "desc"
                      ? "sorted descending"
                      : "sorted ascending"}
                  </Box>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }

  EnhancedTableHead.propTypes = {
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(["asc", "desc"]).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - filterActivities?.length)
      : 0;

  const visibleRows = filterActivities
    ?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    .sort(getComparator(order, orderBy));

  const handleTimelineClick = async (id, timeline) => {
    try {
      await axios.patch(
        `http://localhost:5000/activities/timeline/${id}`,
        {
          timeline: !timeline,
        },
        {
          headers: {
            Authorization: `${jwtToken}`,
            "Content-Type": "application/json",
          },
        }
      );

      setActivity((prev) =>
        prev.map((item) =>
          item.id === id ? { ...item, timeline: !item.timeline } : item
        )
      );
    } catch (error) {
      console.error("Cannot update at the moment", error.message);
    }
  };

  const handleDelete = async (id) => {
    try {
      const response = await axios.delete(
        `http://localhost:5000/activities/${id}`,
        {
          headers: {
            Authorization: `${jwtToken}`,
            "Content-Type": "application/json",
          },
        }
      );
      if (response.data.success) {
        toast.success("Activity Record Deleted!");
        setActivity((prev) => prev.filter((item) => item.id !== id));
      }
    } catch (error) {
      toast.error("Cannot update at the moment");
      console.error("Cannot update at the moment", error.message);
    }
  };

  const handleCheckboxChange = () => {
    setFiltered((prev) => ({ ...prev, topfive: !prev.topfive }));
  };

  React.useEffect(() => {
    setFilterActivities((prev) => {
      const filteredData = activity?.filter((act) =>
        filtered.activity === "none"
          ? true
          : act?.activity === filtered.activity
      );

      if (filtered?.topfive) {
        return filteredData
          ?.sort((a, b) => {
            const A =
              (a.unit === "Kms" ? a.distance * 1000 : a.distance) / a.time;
            const B =
              (b.unit === "Kms" ? b.distance * 1000 : b.distance) / b.time;

            return B - A;
          })

          .slice(0, 5);
      }

      return filteredData;
    });
    // eslint-disable-next-line
  }, [activity, filtered]);

  return (
    <div className="features-main">
      {!isComplete ? (
        <div>
          <IncompleteUserInfo />
        </div>
      ) : activity?.length > 0 ? (
        <Box sx={{ width: "100%" }}>
          <Paper sx={{ width: "100%", mb: 2 }}>
            <Toolbar
              sx={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Typography variant="h5">Performance</Typography>
              <div>
                <FilterListIcon onClick={handleMenuOpen} />
                <Menu
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center",
                  }}
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "center",
                  }}
                  open={open}
                  onClose={handleMenuClose}
                  sx={{ mt: 1 }}
                >
                  <FormControl component="fieldset">
                    <MenuItem>
                      <FormControlLabel
                        value="none"
                        control={<Radio />}
                        label="All"
                        checked={filtered.activity === "none"}
                        onChange={handleRadioChange}
                      />
                    </MenuItem>
                    <MenuItem>
                      <FormControlLabel
                        value="swimming"
                        control={<Radio />}
                        label="Swimming"
                        checked={filtered.activity === "swimming"}
                        onChange={handleRadioChange}
                      />
                    </MenuItem>
                    <MenuItem>
                      <FormControlLabel
                        value="running"
                        control={<Radio />}
                        label="Running"
                        checked={filtered.activity === "running"}
                        onChange={handleRadioChange}
                      />
                    </MenuItem>
                  </FormControl>
                  <MenuItem>
                    <FormControlLabel
                      value="walking"
                      control={<Radio />}
                      label="Walking"
                      checked={filtered.activity === "walking"}
                      onChange={handleRadioChange}
                    />
                  </MenuItem>
                  <MenuItem>
                    <FormControlLabel
                      value="biking"
                      control={<Radio />}
                      label="Biking"
                      checked={filtered.activity === "biking"}
                      onChange={handleRadioChange}
                    />
                  </MenuItem>
                  <MenuItem>
                    <FormControlLabel
                      control={<Checkbox />}
                      label="Top 5"
                      checked={filtered.topfive}
                      onChange={handleCheckboxChange}
                    />
                  </MenuItem>
                </Menu>
              </div>
            </Toolbar>
            <TableContainer>
              <Table
                sx={{ minWidth: 750 }}
                aria-labelledby="tableTitle"
                size={"medium"}
              >
                <EnhancedTableHead
                  order={order}
                  orderBy={orderBy}
                  onRequestSort={handleRequestSort}
                  rowCount={filterActivities?.length || 0}
                />
                <TableBody>
                  {visibleRows?.map((row) => {
                    return (
                      <TableRow key={row.id}>
                        <TableCell align="center">
                          {row.activity.toUpperCase()}
                        </TableCell>
                        <TableCell align="center">{row.distance}</TableCell>
                        <TableCell align="center">{row.unit}</TableCell>
                        <TableCell align="center">{row.time}</TableCell>
                        <TableCell align="center">{row.createdat}</TableCell>
                        <TableCell align="center">
                          <Button
                            style={{
                              backgroundColor: row.timeline ? "red" : "green",
                              color: "white",
                            }}
                            onClick={() =>
                              handleTimelineClick(row.id, row.timeline)
                            }
                          >
                            {row.timeline
                              ? "Remove From TimeLine"
                              : "Add To TimeLine"}
                          </Button>
                        </TableCell>
                        <TableCell align="center">
                          <DeleteIcon
                            sx={{ color: "red", cursor: "pointer" }}
                            onClick={() => handleDelete(row.id)}
                          />
                        </TableCell>
                      </TableRow>
                    );
                  })}
                  {emptyRows > 0 && (
                    <TableRow>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[5, 10]}
              component="div"
              count={filterActivities?.length || 0}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
        </Box>
      ) : (
        <Typography variant="h5">
          No Activities to Show. Record Some in the Activities tab.
        </Typography>
      )}
    </div>
  );
};
