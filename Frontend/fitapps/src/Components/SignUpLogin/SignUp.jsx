import React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import axios from "axios";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import OutlinedInput from "@mui/material/OutlinedInput";
import IconButton from "@mui/material/IconButton";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { FormControl } from "@mui/material";
import validator from "validator";

export default function SignUp() {
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = React.useState(false);
  const [isError, setIsError] = React.useState({ error: false, column: "" });
  const [formError, setFormError] = React.useState({
    phoneNumber: false,
    email: false,
  });

  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    try {
      const obj = {
        phoneNumber: data.get("number").toString().length !== 10 ? true : false,
        email: validator.isEmail(data.get("email")) ? false : true,
      };
      setFormError(obj);
      if (Object.values(obj).includes(true)) {
        throw new Error("Error");
      }
    } catch (error) {
      return;
    }

    try {
      await axios.post("http://localhost:5000/signup", {
        firstName: data.get("firstName"),
        lastName: data.get("lastName"),
        password: data.get("password"),
        phoneNumber: data.get("number"),
        email: data.get("email"),
      });
      toast.success("Registed Successfully! SignIn with Credentials.");
      navigate("/signin");
    } catch (error) {
      console.error("Signup failed:", error.response.data.message);
      const field = error.response.data.message.match(/"([^"]+)"/);
      setIsError({
        error: true,
        column: field ? field[1].toUpperCase() : null,
      });
    }
  };

  return (
    <div>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  error={formError.email}
                  helperText={
                    formError.email ? "Please enter a valid email" : ""
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="phonenumber"
                  label="Phone Number"
                  name="number"
                  type="number"
                  error={formError.phoneNumber}
                  helperText={
                    formError.phoneNumber
                      ? "Phone number must be 10 digits"
                      : ""
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <FormControl variant="outlined" fullWidth>
                  <InputLabel htmlFor="outlined-adornment-password">
                    Password
                  </InputLabel>
                  <OutlinedInput
                    id="outlined-adornment-password"
                    label="Password"
                    required
                    type={showPassword ? "text" : "password"}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    }
                    name="password"
                  />
                </FormControl>
              </Grid>
            </Grid>
            {isError.error && (
              <Typography sx={{ fontSize: "1rem", color: "red", mt: 1 }}>
                {isError.column} already exists!
              </Typography>
            )}

            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign Up
            </Button>

            <Link href="/signin" variant="body2">
              Already have an account? Sign in
            </Link>
          </Box>
        </Box>
      </Container>
    </div>
  );
}
