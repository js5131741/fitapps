import { Button, Grid, Menu, MenuItem } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../Context/AuthContext";
import ProfileImage from "./UserProfile/ProfileImage";
import AccountBoxIcon from "@mui/icons-material/AccountBox";
import LogoutIcon from "@mui/icons-material/Logout";
import { toast } from "react-toastify";

export const Header = () => {
  const navigate = useNavigate();

  const { isLoggedIn, logout, userInfo } = useAuth();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    localStorage.clear();
    navigate(`/`);
    logout();
    handleMenuClose();
    toast.success("Logged out!");
  };
  const handleClick = (route) => {
    if (route === "timeline") {
      navigate(`/${route}/${userInfo.id}`);
    } else {
      navigate(`/${route}`);
    }
  };
  const handleProfileClick = () => {
    handleMenuClose();
    navigate("/profile");
  };

  const handleSignUpClick = () => {
    navigate("/signup");
  };
  const handleLoginClick = () => {
    navigate("/signin");
  };

  const handleLogoClick = () => {
    navigate(`/`);
  };

  return (
    <div style={{ marginBottom: "1rem" }}>
      <Grid
        container
        sx={{
          justifyContent: "space-between",
          alignItems: "center",
          padding: "24px 60px",
          backgroundColor: "white",
        }}
      >
        <Grid item>
          <h1
            onClick={handleLogoClick}
            className="title"
            style={{ cursor: "pointer" }}
          >
            FitApps
          </h1>
        </Grid>
        {isLoggedIn && (
          <Grid item>
            <ul style={{ display: "flex", justifyContent: "center", gap: 2 }}>
              <li onClick={() => handleClick("timeline")}>TIMELINE</li>
              <li onClick={() => handleClick("activities")}>ACTIVITIES</li>
              <li onClick={() => handleClick("performance")}>PERFORMANCE</li>
              <li onClick={() => handleClick("community")}>COMMUNITY</li>
            </ul>
          </Grid>
        )}
        {!isLoggedIn ? (
          <Grid item>
            <Grid container spacing={2}>
              <Grid item>
                <Button variant="contained" onClick={handleSignUpClick}>
                  SignUp
                </Button>
              </Grid>
              <Grid item>
                <Button variant="contained" onClick={handleLoginClick}>
                  SignIn
                </Button>
              </Grid>
            </Grid>
          </Grid>
        ) : (
          <div>
            <ProfileImage
              onClick={handleMenuOpen}
              name={userInfo.firstname + " " + userInfo.lastname}
            />
            <Menu
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "center",
              }}
              transformOrigin={{
                vertical: "top",
                horizontal: "center",
              }}
              open={open}
              onClose={handleMenuClose}
              sx={{ mt: 1 }}
            >
              <MenuItem onClick={handleProfileClick}>
                <AccountBoxIcon sx={{ mr: 2 }} />
                Profile
              </MenuItem>
              <MenuItem onClick={handleLogout}>
                <LogoutIcon sx={{ mr: 2 }} />
                Logout
              </MenuItem>
            </Menu>
          </div>
        )}
      </Grid>
    </div>
  );
};
