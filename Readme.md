### FITAPPS Project

Fitness App to record your various activities and show them on your timeline.

## Getting Started

1. Git clone the project

`git clone https://gitlab.com/js5131741/fitapps.git`

2. Navigate to the directory

`cd FitApps`

3. Install dependencies for both frontend and backend.

#### Frontend:

`cd frontend/fitapps`
`npm install`

#### Backend:

`cd backend`
`npm install`

4. Add a .env file and Start Backend Server

###### .env

```
JWT_SECRET_KEY="your_secret_key"
PORT="server_port"

DATABASE_USER=database_username
DATABASE_HOST=database_host
DATABASE_NAME=database_name
DATABASE_PASSWORD=database_password
DATABASE_PORT=database_server_port
```

###### start server

`node server.js`

5. Start React after navigating to frontend directory

`npm start`
